﻿////////////////////////////////////////////
//
// Initus - Interaction Module
//
// (c) 2020 Bear Talk Studios
//
// Author(s): Alec Pierce
//
////////////////////////////////////////////

// ReSharper disable FieldCanBeMadeReadOnly.Local

using System.Collections.Generic;
using Interactables;
using UnityEngine;
using UnityEngine.Events;
using XRInput;
using Utils.Events;

namespace Interactions
{
    [RequireComponent(typeof(SphereCollider))]
    [RequireComponent(typeof(XRInputManager))]
    public class InteractionManager : MonoBehaviour, IInteractionManager
    {
        //////////////////
        /// PRIVATE API
        //////////////////

        private const float GRIP_RELEASE_THRESHOLD = 0.1f;
        private const float GRIP_PRESS_THRESHOLD = 0.9f;
        
        private const float TRIGGER_RELEASE_THRESHOLD = 0.1f;
        private const float TRIGGER_PRESS_THRESHOLD = 0.9f;

        private bool _isGripPressed;
        private bool _isGripReleased;
        
        private bool _isTriggerPressed;
        private bool _isTriggerReleased;

        private IInteractionEvent _objectGrabbed = new IInteractionEvent();
        private IInteractionEvent _objectReleased = new IInteractionEvent();
        
        private UnityEvent _gripPressed = new UnityEvent();
        private UnityEvent _gripReleased = new UnityEvent();
        
        private UnityEvent _triggerPressed = new UnityEvent();
        private UnityEvent _triggerReleased = new UnityEvent();

        private FloatEvent _rawTriggerPress = new FloatEvent();
        private FloatEvent _rawGripPress = new FloatEvent();
        
        private HashSet<Collider> _objectsTouching = new HashSet<Collider>();
        
        private Collider _closestTouchedCollider;
        private Collider _heldObject;
            
        private XRInputManager _xrInputManager;
        
        private void Awake()
        {
            _xrInputManager = GetComponent<XRInputManager>();

            _xrInputManager.SubscribeToGripPresses(OnGripPress);
            _xrInputManager.SubscribeToTriggerPresses(OnTriggerPress);
            
            _gripPressed.AddListener(OnGrab);
            _gripReleased.AddListener(OnRelease);
        }

        private void OnTriggerStay(Collider other)
        {
            if (_objectsTouching.Contains(other))
            {
                return;
            }

            _objectsTouching.Add(other);
        }

        private void OnTriggerExit(Collider other)
        {
            _objectsTouching.Remove(other);
        }

        private void OnGripPress(float value)
        {
            _rawGripPress.Invoke(value);
            
            if (value < GRIP_RELEASE_THRESHOLD && !_isGripReleased)
            {
                _gripReleased.Invoke();
                _isGripReleased = true;
            }
            else if (value > GRIP_PRESS_THRESHOLD && !_isGripPressed)
            {
                _gripPressed.Invoke();
                _isGripPressed = true;
            }
            else
            {
                _isGripReleased = false;
                _isGripPressed = false;
            }
        }

        private void OnTriggerPress(float value)
        {
            _rawTriggerPress.Invoke(value);
            
            if (value < TRIGGER_RELEASE_THRESHOLD && !_isTriggerReleased)
            {
                _triggerReleased.Invoke();
                _isTriggerReleased = true;
            }
            else if (value > TRIGGER_PRESS_THRESHOLD && !_isTriggerPressed)
            {
                _triggerPressed.Invoke();
                _isTriggerPressed = true;
            }
            else
            {
                _isTriggerReleased = false;
                _isTriggerPressed = false;
            }
        }

        private void OnGrab()
        {
            if (_objectsTouching.Count < 1)
            {
                return;
            }

            foreach (var col in _objectsTouching)
            {
                if (_closestTouchedCollider == null)
                {
                    _closestTouchedCollider = col;
                    continue;
                }

                var position = gameObject.transform.position;
                var closestMag = Mathf.Abs(Vector3.Magnitude(_closestTouchedCollider.gameObject.transform.position - position));
                var currentMag = Mathf.Abs(Vector3.Magnitude(col.gameObject.transform.position - position));

                if (currentMag < closestMag)
                {
                    _closestTouchedCollider = col;
                }
            }

            _heldObject = _closestTouchedCollider;
            if (_heldObject.gameObject.TryGetComponent(out IStaticInteractable interactable))
            {
                interactable.ObjectGrabbed(this);
            }
        }

        private void OnRelease()
        {
            _heldObject = null;
            _objectReleased.Invoke(this);
        }

        //////////////////
        /// PUBLIC API
        //////////////////
        
        public void SubscribeToGrabs(UnityAction<IInteractionManager> grabCallback) => 
            _objectGrabbed.AddListener(grabCallback);
        
        public void SubscribeToReleases(UnityAction<IInteractionManager> releaseCallback) => 
            _objectReleased.AddListener(releaseCallback);

        public void SubscribeToRawGripChanges(UnityAction<float> gripCallback) => 
            _rawGripPress.AddListener(gripCallback);
        
        public void SubscribeToRawTriggerChanges(UnityAction<float> triggerCallback) => 
            _rawTriggerPress.AddListener(triggerCallback);

        public void SubscribeToGripPresses(UnityAction gripCallback) => 
            _gripPressed.AddListener(gripCallback);
        
        public void SubscribeToGripReleases(UnityAction gripCallback) => 
            _gripReleased.AddListener(gripCallback);

        public void SubscribeToTriggerPresses(UnityAction triggerCallback) =>
            _triggerPressed.AddListener(triggerCallback);

        public void SubscribeToTriggerReleases(UnityAction triggerCallback) =>
            _triggerReleased.AddListener(triggerCallback);

        public void UnsubscribeFromGrabs(UnityAction<IInteractionManager> grabCallback) =>
            _objectGrabbed.RemoveListener(grabCallback);
        
        public void UnsubscribeFromReleases(UnityAction<IInteractionManager> releaseCallback) => 
            _objectReleased.RemoveListener(releaseCallback);
        
        public void UnsubscribeFromRawGripChanges(UnityAction<float> gripCallback) => 
            _rawGripPress.RemoveListener(gripCallback);
        
        public void UnsubscribeFromRawTriggerChanges(UnityAction<float> triggerCallback) => 
            _rawTriggerPress.RemoveListener(triggerCallback);

        public void UnsubscribeFromGripPresses(UnityAction gripCallback) =>
            _gripPressed.RemoveListener(gripCallback);

        public void UnsubscribeFromGripReleases(UnityAction gripCallback) =>
            _gripReleased.RemoveListener(gripCallback);

        public void UnsubscribeFromTriggerPresses(UnityAction triggerCallback) =>
            _triggerPressed.RemoveListener(triggerCallback);

        public void UnsubscribeFromTriggerReleases(UnityAction triggerCallback) =>
            _triggerReleased.RemoveListener(triggerCallback);

        public Transform GetTransform => gameObject.transform;
    }
}