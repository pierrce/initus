﻿////////////////////////////////////////////
//
// Initus - Interaction Module
//
// (c) 2020 Bear Talk Studios
//
// Author(s): Alec Pierce
//
////////////////////////////////////////////

// ReSharper disable UnusedMember.Global

using UnityEngine;
using UnityEngine.Events;

namespace Interactions
{
    public interface IInteractionManager
    {
        void SubscribeToGrabs(UnityAction<IInteractionManager> grabCallback);
        void SubscribeToReleases(UnityAction<IInteractionManager> releaseCallback);
        void SubscribeToRawGripChanges(UnityAction<float> gripCallback);
        void SubscribeToRawTriggerChanges(UnityAction<float> triggerCallback);
        void SubscribeToGripPresses(UnityAction gripCallback);
        void SubscribeToGripReleases(UnityAction gripCallback);
        void SubscribeToTriggerPresses(UnityAction triggerCallback);
        void SubscribeToTriggerReleases(UnityAction triggerCallback);
        void UnsubscribeFromRawGripChanges(UnityAction<float> gripCallback);
        void UnsubscribeFromRawTriggerChanges(UnityAction<float> triggerCallback);
        void UnsubscribeFromGripPresses(UnityAction gripCallback);
        void UnsubscribeFromGripReleases(UnityAction gripCallback);
        void UnsubscribeFromTriggerPresses(UnityAction triggerCallback);
        void UnsubscribeFromTriggerReleases(UnityAction triggerCallback);
        void UnsubscribeFromGrabs(UnityAction<IInteractionManager> grabCallback);
        void UnsubscribeFromReleases(UnityAction<IInteractionManager> releaseCallback);
        Transform GetTransform { get; }
    }
}

