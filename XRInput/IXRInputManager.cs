﻿////////////////////////////////////////////
//
// Initus - XRInput Module
//
// (c) 2020 Bear Talk Studios
//
// Author(s): Alec Pierce
//
////////////////////////////////////////////

// ReSharper disable UnusedMember.Global

using System;
using UnityEngine;

namespace XRInput
{
    public interface IXRInputManager
    {
        void SubscribeToTriggerPresses(Action<float> triggerCallback);
        void SubscribeToGripPresses(Action<float> gripCallback);
        void SubscribeToDPadPresses(Action<Vector2> dPadCallback);
        void UnsubscribeFromTriggerPresses(Action<float> triggerCallback);
        void UnsubscribeFromGripPresses(Action<float> gripCallback);
        void UnsubscribeFromDPadPresses(Action<Vector2> dPadCallback);
    }
}
