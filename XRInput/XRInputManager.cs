﻿////////////////////////////////////////////
//
// Initus - XRInput Module
//
// (c) 2020 Bear Talk Studios
//
// Author(s): Alec Pierce
//
////////////////////////////////////////////

// ReSharper disable UnusedMember.Global

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Utils.Events;

namespace XRInput
{
    public class XRInputManager : MonoBehaviour, IXRInputManager
    {
        //////////////////
        /// PRIVATE API
        //////////////////
        
        [SerializeField] private InputDeviceCharacteristics _characteristic;
        [SerializeField] private  GameObject _nodeObject;

        /* Keep this around to avoid creating heap garbage */
        private static readonly List<InputDevice> devices = new List<InputDevice>();

        private readonly Vector2Event _dPadEvent = new Vector2Event();
        private readonly FloatEvent _triggerEvent = new FloatEvent();
        private readonly FloatEvent _gripEvent = new FloatEvent();

        private void Update()
        {
            InputDevices.GetDevicesWithCharacteristics(_characteristic, devices);

            if (devices.Count <= 0)
            {
                return;
            }

            var device = devices[0];

            CalculateTransform(device);
            CheckForInputs(device);
        }

        private void Awake()
        {
            if (!_nodeObject)
            {
                _nodeObject = gameObject;
            }
        }

        private void CalculateTransform(InputDevice device)
        {
            if (device.TryGetFeatureValue(CommonUsages.devicePosition, out var position))
            {
                transform.localPosition = position;
            }

            if (device.TryGetFeatureValue(CommonUsages.deviceRotation, out var rotation))
            {
                transform.localRotation = rotation;
            }
        }

        private void CheckForInputs(InputDevice device)
        {
            if (device.TryGetFeatureValue(CommonUsages.primary2DAxis, out var dPad))
            {
                _dPadEvent.Invoke(dPad);
            }

            if (device.TryGetFeatureValue(CommonUsages.trigger, out var trigger))
            {
                _triggerEvent.Invoke(trigger);
            }

            if (device.TryGetFeatureValue(CommonUsages.grip, out var grip))
            {
                _gripEvent.Invoke(grip);
            }
        }
        
        //////////////////
        /// PUBLIC API
        //////////////////

        public void SubscribeToTriggerPresses(Action<float> triggerCallback)
        {
            _triggerEvent.AddListener(value => triggerCallback(value));
        }

        public void SubscribeToGripPresses(Action<float> gripCallback)
        {
            _gripEvent.AddListener(value => gripCallback(value));
        }

        public void SubscribeToDPadPresses(Action<Vector2> dPadCallback)
        {
            _dPadEvent.AddListener(value => dPadCallback(value));
        }

        public void UnsubscribeFromTriggerPresses(Action<float> triggerCallback)
        {
            _triggerEvent.RemoveListener(value => triggerCallback(value));
        }

        public void UnsubscribeFromGripPresses(Action<float> gripCallback)
        {
            _gripEvent.RemoveListener(value => gripCallback(value));
        }

        public void UnsubscribeFromDPadPresses(Action<Vector2> dPadCallback)
        {
            _dPadEvent.RemoveListener(value => dPadCallback(value));
        }
    }
}
