﻿////////////////////////////////////////////
//
// Initus - Utils Module
//
// (c) 2020 Bear Talk Studios
//
// Author(s): Alec Pierce
//
////////////////////////////////////////////

// ReSharper disable UnusedType.Global

using Interactions;
using UnityEngine;
using UnityEngine.Events;

namespace Utils.Events
{
    public class ColliderEvent : UnityEvent<Collider> { }
    public class GameObjectEvent : UnityEvent<GameObject> { }
    public class StringEvent : UnityEvent<string> { }
    public class FloatEvent : UnityEvent<float> { }
    public class IntEvent : UnityEvent<int> { }
    public class Vector2Event : UnityEvent<Vector2> { }
    public class IInteractionEvent : UnityEvent<IInteractionManager> { }
}
