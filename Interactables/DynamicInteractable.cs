﻿////////////////////////////////////////////
//
// Initus - Interactable Module
//
// (c) 2020 Bear Talk Studios
//
// Author(s): Alec Pierce
//
////////////////////////////////////////////

// ReSharper disable Unity.InefficientPropertyAccess

using Interactions;
using UnityEngine;

namespace Interactables
{
    [RequireComponent(typeof(Rigidbody))]
    public class DynamicInteractable : StaticInteractable, IDynamicInteractable
    {
        private Rigidbody _physicsBody;
        private bool _isBeingHeld;

        private Vector3 _lastRotation;
        private Vector3 _rotationDelta;
        
        private Vector3 _lastPosition;
        private Vector3 _positionDelta;

        private void Awake()
        {
            _physicsBody = GetComponent<Rigidbody>();
            _lastPosition = transform.position;
            _lastRotation = transform.rotation.eulerAngles;
            
            EnablePhysics(true);
        }
    
        private void Update()
        {
            if (!_isBeingHeld)
            {
                return;
            }

            var rotation = transform.rotation;
            _rotationDelta = (rotation.eulerAngles - _lastRotation) / Time.deltaTime;
            _lastRotation = rotation.eulerAngles;
            
            var position = transform.position;
            _positionDelta = (position - _lastPosition) / Time.deltaTime;
            _lastPosition = position;
        }

        private void EnablePhysics(bool useGravity)
        {
            _physicsBody.useGravity = useGravity;
            _physicsBody.isKinematic = !useGravity;

            if (!useGravity)
            {
                return;
            }
            
            _physicsBody.velocity = _positionDelta;
            _physicsBody.angularVelocity = _rotationDelta;
        }

        public override void ObjectGrabbed(IInteractionManager interactionManager)
        {
            base.ObjectGrabbed(interactionManager);

            _isBeingHeld = true;
            transform.parent = interactionManager.GetTransform;
            
            EnablePhysics(false);
        }

        public override void ObjectReleased(IInteractionManager interactionManager)
        {
            base.ObjectReleased(interactionManager);

            _isBeingHeld = false;
            transform.parent = null;

            EnablePhysics(true);
        }
    }
}

