﻿////////////////////////////////////////////
//
// Initus - Interactable Module
//
// (c) 2020 Bear Talk Studios
//
// Author(s): Alec Pierce
//
////////////////////////////////////////////

using Interactions;

namespace Interactables
{
    public interface IStaticInteractable
    {
        void ObjectGrabbed(IInteractionManager interactionManager);
        void ObjectReleased(IInteractionManager interactionManager);
    }
}

