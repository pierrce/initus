﻿////////////////////////////////////////////
//
// Initus - Interaction Module
//
// (c) 2020 Bear Talk Studios
//
// Author(s): Alec Pierce
//
////////////////////////////////////////////

using Interactions;
using UnityEngine;

namespace Interactables
{
    public class StaticInteractable : MonoBehaviour, IStaticInteractable
    {
        public virtual void ObjectGrabbed(IInteractionManager interactionManager)
        {
            interactionManager.SubscribeToReleases(ObjectReleased);
        }

        public virtual void ObjectReleased(IInteractionManager interactionManager)
        {
            interactionManager.UnsubscribeFromReleases(ObjectReleased);
        }
    }
}

